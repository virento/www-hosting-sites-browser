package barszczewski.kacper.freeHosting;

import java.util.List;

/**
 * Created by namis on 3/11/2019.
 * LetMeIn
 */
class Site {

    private String title;
    private String[] features = new String[4];
    private String[] featuresList;
    private String[] domains;
    private String[] previewImages;
    private String description;
    private String www = "www.podaj.strone.pl";
    private String planWWW = "";
    private String description1 = "", description2 = "";
    private String iconUrl;
    private int addTime = 0;

    public Site(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Site() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String[] getFeatures() {
        return features;
    }

    public void setFeatures(String[] features) {
        this.features = features;
    }

    public void setFeatuer(String feature, int index){
        this.features[index] = feature;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getWWW() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public String[] getDomains() {
        return domains == null ? new String[0] : domains;
    }

    public void setDomains(String[] domains) {
        this.domains = domains;
    }

    public String getDescription1() {
        return description1;
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription1(String description1) {
        if(description1 == null) return;
        this.description1 = description1;
    }

    public void setDescription2(String description2) {
        if(description2 == null) return;
        this.description2 = description2;
    }

    public void setFeaturesList(String[] featureList) {
        this.featuresList = featureList;
    }

    public String[] getFeaturesList() {
        return featuresList == null ? new String[0] : featuresList;
    }

    public void setIconUrl(String icon) {
        this.iconUrl = icon;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String[] getPreviewImages() {
        return previewImages == null ? new String[0] : previewImages;
    }

    public void setPreviewImages(String[] previewImages) {
        this.previewImages = previewImages;
    }

    public void setAddTime(int addTime) {
        this.addTime = addTime;
    }

    public int getAddTime() {
        return addTime;
    }

    public String getPlanWWW() {
        return planWWW;
    }

    public void setPlanWWW(String planWWW) {
        this.planWWW = planWWW;
    }
}
