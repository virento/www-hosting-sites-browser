package barszczewski.kacper.freeHosting;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Spinner;


import java.util.ArrayList;
import java.util.Collections;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by namis on 3/11/2019.
 * LetMeIn
 */
interface OnSiteItemClickListener {
    void onSiteItemClick(View view, Site site, String transitionName);
}

public class FragmentSiteList extends Fragment implements OnSiteItemClickListener {

    private static String EXTRA_SCROLL_POSITION = "barszczewski.kacper.freeHosting.SCROLL_POSITION";
    public static String EXTRA_TRANSITION_NAME = "barszczewski.kacper.freeHosting.TRANSITION_NAME";

    public static FragmentSiteList newInstance(ArrayList<Site> sitesList) {
        FragmentSiteList fragment = new FragmentSiteList();
        fragment.sitesList = sitesList;
        return fragment;
    }

    private ArrayList<Site> sitesList;

    private RecyclerView mSitesRecyclerView;
    private SitesListAdapter mSiteListAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root =  inflater.inflate(R.layout.content_scrolling, container, false);

        if (getArguments() == null) {
            setArguments(new Bundle());
        }

        Spinner s = root.findViewById(R.id.sort_spinner);
        if (s != null) {
            s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                int check = 0;

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (view != null && check++ > 0) {
                        new SortAsyncTask(position).execute(sitesList);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        mSitesRecyclerView = root.findViewById(R.id.sites_list);
        mSitesRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mSitesRecyclerView.setLayoutManager(layoutManager);

        mSiteListAdapter = new SitesListAdapter(this, this);
        mSiteListAdapter.setItems(sitesList);
        mSitesRecyclerView.setAdapter(mSiteListAdapter);

        if (sitesList.size() > 0) {
            root.findViewById(R.id.sites_progressbar).setVisibility(View.INVISIBLE);
            mSitesRecyclerView.setVisibility(View.VISIBLE);
        }

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((ScrollingActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((ScrollingActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    private View findViewById(int id) {
        Activity activity = getActivity();
        if (activity == null) return null;
        return activity.findViewById(id);
    }

    @Override
    public void onSiteItemClick(View view, Site site, String transitionName) {
        Activity activity = getActivity();
        if (activity != null) {

            RecyclerView.LayoutManager layoutManager = mSitesRecyclerView.getLayoutManager();
            if (layoutManager != null) {
                if (getArguments() != null) {
                    Parcelable p = layoutManager.onSaveInstanceState();
                    getArguments().putParcelable(FragmentSiteList.EXTRA_SCROLL_POSITION, p);
                }
            }

            ActivityInterface ai = (ActivityInterface) activity;
//            Fade exitFade = new Fade();
//            exitFade.setDuration(500);
//            setExitTransition(exitFade);
            ai.showSiteFragment(view, site, transitionName);
        }
    }

    public void notifyNewSites() {
        if (mSiteListAdapter != null) {
            mSiteListAdapter.notifyDataSetChanged();
        }
        if (mSitesRecyclerView.getVisibility() == View.INVISIBLE && mSiteListAdapter.getItemCount() > 0) {
            ProgressBar bar = (ProgressBar) findViewById(R.id.sites_progressbar);
            if (bar != null) bar.setVisibility(View.INVISIBLE);
            mSitesRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    class SortAsyncTask extends AsyncTask<ArrayList<Site>, Void, ArrayList<Site>> {

        private int sortType;

        SortAsyncTask(int sortType) {
            this.sortType = sortType;
        }

        @Override
        protected void onPreExecute() {
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.sites_progressbar);
            if (progressBar != null) {
                progressBar.setVisibility(View.VISIBLE);
            }
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.sites_list);
            if (recyclerView != null) {
                recyclerView.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        protected ArrayList<Site> doInBackground(ArrayList<Site>... arrayLists) {
            for (ArrayList<Site> list : arrayLists) {
                switch (sortType) {
                    case 0: /* Newest */
                        Collections.sort(list, (o1, o2) -> o1.getAddTime() - o2.getAddTime());
                        return list;
                    case 1: /* Name (A-Z) */
                        Collections.sort(list, (o1, o2) -> o1.getTitle().compareTo(o2.getTitle()));
                        return list;
                    case 2: /* Name (Z-A) */
                        Collections.sort(list, (o1, o2) -> -o1.getTitle().compareTo(o2.getTitle()));
                        return list;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Site> sites) {
            notifyNewSites();
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.sites_progressbar);
            if (progressBar != null) {
                progressBar.setVisibility(View.INVISIBLE);
            }
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.sites_list);
            if (recyclerView != null) {
                recyclerView.setVisibility(View.VISIBLE);
            }
        }
    }
}
