package barszczewski.kacper.freeHosting;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.BulletSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Created by namis on 3/11/2019.
 * LetMeIn
 */
public class FragmentSite extends Fragment {

    public static FragmentSite newInstance(Site site) {
        FragmentSite fragment = new FragmentSite();
        fragment.site = site;
        return fragment;
    }

    private Site site;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root =  inflater.inflate(R.layout.content_site, container, false);

        TextView description1 = root.findViewById(R.id.site_description1);
        TextView description2 = root.findViewById(R.id.site_description2);
        TextView foot = root.findViewById(R.id.site_foot);
        TextView titleView = root.findViewById(R.id.site_title);
        TextView wwwView = root.findViewById(R.id.site_www);
        TextView domainsView = root.findViewById(R.id.site_domains);
        TextView domainsTitleView = root.findViewById(R.id.site_domains_title);
        TextView featuresView = root.findViewById(R.id.site_features);
        TextView featuresTitleView = root.findViewById(R.id.site_feature_title);
        ImageView logoView = root.findViewById(R.id.site_img);
        Button openWebsite = root.findViewById(R.id.site_open_button);
        View siteSeparator = root.findViewById(R.id.site_separator2);

        if (getArguments() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                String transitionName = getArguments().getString(FragmentSiteList.EXTRA_TRANSITION_NAME);
                logoView.setTransitionName(transitionName);
            }
        }

        foot.setText(getString(R.string.site_foot, site.getWWW(), site.getPlanWWW()));

        if (site.getDescription1().length() > 0) {
            description1.setText(site.getDescription1());
            description1.setVisibility(View.VISIBLE);
        } else {
            description1.setVisibility(View.GONE);
        }

        if (site.getDescription2().length() > 0) {
            description2.setText(site.getDescription2());
            description2.setVisibility(View.VISIBLE);
        } else {
            description2.setVisibility(View.GONE);
        }
        if (site.getDescription1().length() == 0
                && site.getDescription2().length() == 0) {
            siteSeparator.setVisibility(View.INVISIBLE);
        } else {
            siteSeparator.setVisibility(View.VISIBLE);
        }
        titleView.setText(site.getTitle());
        wwwView.setMovementMethod(LinkMovementMethod.getInstance());
        wwwView.setText(site.getWWW());

        String[] domains = site.getDomains();
        if (domains.length > 0) {
            SpannableStringBuilder builder = new SpannableStringBuilder();
            for (String domain : site.getDomains()) {
                int length = builder.length();
                builder.append(domain).append("\n");
                builder.setSpan(new BulletSpan(30), length, length + 1,
                        Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            }
            domainsView.setText(builder);
            domainsView.setVisibility(View.VISIBLE);
            domainsTitleView.setVisibility(View.VISIBLE);
        } else {
            domainsView.setVisibility(View.GONE);
            domainsTitleView.setVisibility(View.GONE);
        }

        String[] featureList = site.getFeaturesList();
        if (featureList.length > 0) {
            SpannableStringBuilder builder = new SpannableStringBuilder();
            for (String feature : site.getFeaturesList()) {
                int length = builder.length();
                builder.append(feature).append("\n");
                builder.setSpan(new BulletSpan(30), length, length + 1,
                        Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            }
            featuresView.setText(builder);
            featuresView.setVisibility(View.VISIBLE);
            featuresTitleView.setVisibility(View.VISIBLE);
        } else {
            featuresView.setVisibility(View.GONE);
            featuresTitleView.setVisibility(View.GONE);
        }

        String[] previewImages = site.getPreviewImages();
        if (previewImages.length == 0) {
            root.findViewById(R.id.site_previews_title).setVisibility(View.INVISIBLE);
        } else {
            root.findViewById(R.id.site_previews_title).setVisibility(View.VISIBLE);
        }
        for (int i = 0; i < previewImages.length; i++) {
            ImageView imageView;
            if (i == 0) imageView = root.findViewById(R.id.previewImage1);
            else if (i == 1) imageView = root.findViewById(R.id.previewImage2);
            else if (i == 2) imageView = root.findViewById(R.id.previewImage3);
            else continue;
            imageView.setVisibility(View.VISIBLE);
            Glide.with(this)
                    .load(previewImages[i])
                    .fitCenter()
                    .placeholder(R.drawable.img_loading)
                    .into(imageView);
        }

        Glide.with(this)
                .load(site.getIconUrl())
                .placeholder(R.drawable.no_site_logo)
                .fitCenter()
                .into(logoView);

        openWebsite.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(site.getWWW()));
            startActivity(browserIntent);
        });

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((ScrollingActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((ScrollingActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppBarLayout) findViewById(R.id.app_bar)).setExpanded(true, false);
    }

    private View findViewById(int id) {
        Activity activity = getActivity();
        if (activity == null) return null;
        return activity.findViewById(id);
    }
}
