package barszczewski.kacper.freeHosting;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by namis on 3/11/2019.
 * LetMeIn
 */
class SitesListAdapter extends RecyclerView.Adapter<SitesListAdapter.ViewHolder> {
    private ArrayList<Site> itemsList = new ArrayList<>();

    private OnSiteItemClickListener onSiteItemClickListener;

    private Fragment fragment;

    SitesListAdapter(Fragment fragment, OnSiteItemClickListener onSiteItemClickListener) {
        this.onSiteItemClickListener = onSiteItemClickListener;
        this.fragment = fragment;
    }


    public void setItems(ArrayList<Site> itemsList) {
        this.itemsList = itemsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_sites_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int mPosition = holder.getAdapterPosition();
        Site site = itemsList.get(mPosition);
        holder.titleView.setText(site.getTitle());
        String www = site.getWWW();
        if (www.charAt(www.length() - 1) == '/') ;
        int startIndex = www.indexOf("://") + 3;
        int endIndex = www.charAt(www.length() - 1) == '/' ? www.length() - 1 : www.length();
        www = www.substring(startIndex, endIndex);
        holder.descriptionView.setText(www);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.logo.setTransitionName("transition" + mPosition);
        }
        holder.view.setOnClickListener(v -> {
            onSiteItemClickListener.onSiteItemClick(holder.logo, site, "transition" + mPosition);
        });
        Glide.with(fragment)
                .load(site.getIconUrl())
                .placeholder(R.drawable.no_site_logo)
                .fitCenter()
                .into(holder.logo);

        String[] featuresList = site.getFeatures();
        int numbOfRow = featuresList.length;
        int i;
        for (i = 0; i < numbOfRow; i++) {
            if (featuresList[i].length() == 0) {
                i--;
                break;
            }
            holder.featuresView[i].setText(featuresList[i]);
            holder.rowView[i].setVisibility(View.VISIBLE);
        }
        if (i < 0) i = 0;
        for (; i < holder.rowView.length; i++) {
            holder.rowView[i].setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView titleView, descriptionView;
        private TextView[] featuresView = new TextView[4];
        private TableRow[] rowView = new TableRow[4];
        private View view;
        private ImageView logo;

        ViewHolder(@NonNull View view) {
            super(view);
            this.view = view;
            this.titleView = view.findViewById(R.id.rowTitle);
            this.descriptionView = view.findViewById(R.id.rowDescription);
            this.logo = view.findViewById(R.id.rowLogo);
            featuresView[0] = view.findViewById(R.id.rowFeature1);
            featuresView[1] = view.findViewById(R.id.rowFeature2);
            featuresView[2] = view.findViewById(R.id.rowFeature3);
            featuresView[3] = view.findViewById(R.id.rowFeature4);
            rowView[0] = view.findViewById(R.id.rowTableFeature1);
            rowView[1] = view.findViewById(R.id.rowTableFeature2);
            rowView[2] = view.findViewById(R.id.rowTableFeature3);
            rowView[3] = view.findViewById(R.id.rowTableFeature4);
        }
    }
}
