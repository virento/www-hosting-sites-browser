package barszczewski.kacper.freeHosting;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

interface ActivityInterface {
    void showSiteFragment(View sharedImageView, Site site, String sharedElementName);
}

public class ScrollingActivity extends AppCompatActivity implements ActivityInterface {

    public static String EXTRA_SCROOL_X = "barszczewski.kacper.freeHosting.SCROLL_X";
    public static String EXTRA_SCROOL_Y = "barszczewski.kacper.freeHosting.SCROLL_Y";

    private ArrayList<Site> sitesList = new ArrayList<>();
    private NestedScrollView mNestedScrollView;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        populateSiteList(sitesList);

        FragmentSiteList fragmentSiteList = FragmentSiteList.newInstance(sitesList);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_view, fragmentSiteList, "").commit();

        mAdView = findViewById(R.id.adView);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                if (mAdView.getVisibility() != View.VISIBLE) {
                    mAdView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAdFailedToLoad(int i) {
                if (mAdView.getVisibility() == View.VISIBLE) {
                    mAdView.setVisibility(View.GONE);
                }
            }
        });

        MobileAds.initialize(this, "ca-app-pub-1350821116154148~9507842011");
        mAdView.loadAd(getAdRequest());
    }

    private void populateSiteList(ArrayList<Site> sitesList) {
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        Task<QuerySnapshot> snap = firestore.collection("sites").get();
        snap.addOnSuccessListener(command -> {
            List<DocumentSnapshot> list = command.getDocuments();
            for (DocumentSnapshot documentSnapshot : list) {
                if (documentSnapshot.contains("hide")
                        && documentSnapshot.getBoolean("hide")) {
                    continue;
                }

                Site site = new Site(documentSnapshot.getString("name"), "Description");
                site.setWww(documentSnapshot.getString("www"));
                site.setPlanWWW(documentSnapshot.getString("plan_www"));
                site.setDescription1(documentSnapshot.getString("description1"));
                site.setDescription2(documentSnapshot.getString("description2"));
                site.setIconUrl(documentSnapshot.getString("icon"));

//                site.setFeatures(new String[]{
//                        documentSnapshot.getString("feature1"),
//                        documentSnapshot.getString("feature2"),
//                        documentSnapshot.getString("feature3"),
//                        documentSnapshot.getString("feature4")
//                });
//
                List<String> features = (List<String>) documentSnapshot.get("features");
                if(features != null) {
                    site.setFeatures(features.toArray(new String[0]));
                }

                List<String> domains = (List<String>) documentSnapshot.get("domains");
                if (domains != null) {
                    site.setDomains(domains.toArray(new String[0]));
                }
                List<String> featureList = (List<String>) documentSnapshot.get("plan_list");
                if (featureList != null) {
                    site.setFeaturesList(featureList.toArray(new String[0]));
                }
                List<String> previewsList = (List<String>) documentSnapshot.get("previews");
                if (previewsList != null) {
                    site.setPreviewImages(previewsList.toArray(new String[0]));
                }

                sitesList.add(site);
                Log.d("FIRESTORE", "New site: " + site.getTitle() + " , " + site.getWWW());
            }
            notifyNewSites();
        });
        snap.addOnFailureListener(command -> {
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle(R.string.error)
                    .setMessage(R.string.cant_download_hostings)
                    .setPositiveButton(android.R.string.ok, (dialog1, which) -> dialog1.dismiss())
                    .create();
            dialog.show();
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void notifyNewSites() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_view);
        if (fragment instanceof FragmentSiteList) {
            ((FragmentSiteList) fragment).notifyNewSites();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            Log.d("TEST", "Back pressed");
            getSupportFragmentManager().popBackStackImmediate();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showSiteFragment(View sharedImageView, Site site, String sharedElementName) {
        FragmentSite fragment = FragmentSite.newInstance(site);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Fragment current = getSupportFragmentManager().findFragmentById(R.id.fragment_view);
            if (current != null) {
                current.setSharedElementReturnTransition(TransitionInflater.from(this)
                        .inflateTransition(android.R.transition.no_transition));
                current.setExitTransition(TransitionInflater.from(this)
                        .inflateTransition(android.R.transition.no_transition));

                fragment.setSharedElementEnterTransition(TransitionInflater.from(this)
                        .inflateTransition(R.transition.image_transition));
                fragment.setEnterTransition(TransitionInflater.from(this)
                        .inflateTransition(android.R.transition.no_transition));

                Bundle bundle = new Bundle();
                bundle.putString(FragmentSiteList.EXTRA_TRANSITION_NAME, sharedElementName);
                fragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_view, fragment, "")
                        .addToBackStack(null)
                        .addSharedElement(sharedImageView, sharedElementName)
                        .commit();
                return;
            }

        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_view, fragment, "")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().popBackStackImmediate()) {
            return;
        }

        super.onBackPressed();
    }

    private AdRequest getAdRequest() {
        return new AdRequest.Builder()
                .addTestDevice("27CEEAD7EDF85D1876D8E7579B5E3779")
                .addTestDevice("97E28D8208F01C08D62DCE19DBD7B515")
                .addTestDevice("D09E762A7B56DE476A6874968471F89C")
                .build();
    }
}
